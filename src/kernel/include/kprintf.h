#ifndef KPRINTF_H
#define KPRINTF_H

extern void kprintf(char * format, ...);
extern void kaprintf(char * format, int ** args);

#endif /* ndef KPRINTF_H */
