/*
 * Copyright (c) 2008 The tyndur Project. All rights reserved.
 *
 * This code is derived from software contributed to the tyndur Project
 * by Antoine Kaufmann.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *     This product includes software developed by the tyndur Project
 *     and its contributors.
 * 4. Neither the name of the tyndur Project nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _KEYMAP_H_
#define _KEYMAP_H_
#include <stdint.h>
#include <stddef.h>
#include <lostio.h>

#define KEYMAP_DEADKEY 0x80000000

/**
 * Eintrag in der Keymap
 */
typedef struct keymap_entry {
    wchar_t normal;
    wchar_t shift;
    wchar_t altgr;
    wchar_t ctrl;
} keymap_entry_t;

extern keymap_entry_t keymap[128];

/**
 * Eintrag aus der Keymap auslesen
 *
 * @return Pointer auf den Eintrag oder NULL wenn keiner existiert
 */
keymap_entry_t* keymap_get(uint8_t keycode);

/**
 * Legt die vterm:/keymap im Verzeichnisbaum an
 */
void keymap_init(void);

/**
 * LostIO-Handller um die Tastaturbelegung zu lesen
 */
size_t keymap_read(lostio_filehandle_t* file, void* buf,
    size_t blocksize, size_t blockcount);

/**
 * LostIO-Handler um die Tastaturbelegung zu aendern
 */
size_t keymap_write(lostio_filehandle_t* file, size_t blocksize,
    size_t blockcount, void* data);

#endif // ifndef _KEYMAP_H_

