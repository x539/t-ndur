#ifndef TEST_H
#define TEST_H

void passed(void);
void failed(void);
void check(int condition);

#endif
