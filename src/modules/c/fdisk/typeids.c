/*
 * Copyright (c) 2008 The tyndur Project. All rights reserved.
 *
 * This code is derived from software contributed to the tyndur Project
 * by Martin Kleusberg.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *     This product includes software developed by the tyndur Project
 *     and its contributors.
 * 4. Neither the name of the tyndur Project nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "typeids.h"

/** 
 * Enthaelt die Zuordnung von Namen zu allen moeglichen Partitionstypen.
 *
 * Die Namen duerfen momenten nicht laenger als 21 sein,
 * siehe shell_typelist() in fdisk.c
 *
 * Siehe http://www.win.tue.nl/~aeb/partitions/partition_types-1.html
 */
// TODO Namen und Abkuerzungen vereinheitlichen
struct partition_type_id_t partition_type_id[257] = {
    {0x00, "No Part."},
    {0x01, "FAT12"},
    {0x02, "XENIX root"},
    {0x03, "XENIX user"},
    {0x04, "FAT16 <32MB"},
    {0x05, "Extended"},
    {0x06, "FAT16 >32MB"},
    {0x07, "NTFS/HPFS"},
    {0x08, "AIX"},
    {0x09, "AIX bootable"},
    {0x0A, "OS/2 Boot Manag"},
    {0x0B, "FAT32"},
    {0x0C, "FAT32 (LBA)"},
    {0x0D, 0},
    {0x0E, "FAT16 (LBA)"},
    {0x0F, "W95 Ext' LBA"},
    {0x10, "OPUS"},
    {0x11, "FAT12 (Hidden)"},
    {0x12, "Diagnostic"},
    {0x13, 0},
    {0x14, "FAT16 <32M hidn"},
    {0x15, 0},
    {0x16, "FAT16 >32M hidn"},
    {0x17, "NTFS/HPFS hid'n"},
    {0x18, "AST SmartSleep"},
    {0x19, 0},
    {0x1A, 0},
    {0x1B, "FAT32 hidden"},
    {0x1C, "FAT32 hid'n LBA"},
    {0x1D, 0},
    {0x1E, "FAT16 hid'n LBA"},
    {0x1F, 0},
    {0x20, 0},
    {0x21, 0},
    {0x22, 0},
    {0x23, 0},
    {0x24, "NEC DOS"},
    {0x25, 0},
    {0x26, 0},
    {0x27, 0},
    {0x28, 0},
    {0x29, 0},
    {0x2A, "AtheOS AFS"},
    {0x2B, "SyllableSecure"},
    {0x2C, 0},
    {0x2D, 0},
    {0x2E, 0},
    {0x2F, 0},
    {0x30, 0},
    {0x31, 0},
    {0x32, "NOS"},
    {0x33, 0},
    {0x34, 0},
    {0x35, "JFS on OS/2"},
    {0x36, 0},
    {0x37, 0},
    {0x38, "THEOS 2GB"},
    {0x39, "Plan9"},
    {0x3A, "THEOS 4GB"},
    {0x3B, "THEOS extended"},
    {0x3C, "PartitionMagic"},
    {0x3D, "NetWare Hidden"},
    {0x3E, 0},
    {0x3F, 0},
    {0x40, "Venix 80286"},
    {0x41, "PPC PReP Boot"},
    {0x42, "SFS"},
    {0x43, 0},
    {0x44, "GoBack"},
    {0x45, "Priam"},
    {0x46, "Eumel/Ergos L3"},
    {0x47, "Eumel/Ergos L3"},
    {0x48, "Eumel/Ergos L3"},
    {0x49, 0},
    {0x4A, "AdaOS"},
    {0x4B, 0},
    {0x4C, "Oberon"},
    {0x4D, "QNX4.x"},
    {0x4E, "QNX4.x 2nd part"},
    {0x4F, "QNX4.x 3rd part"},
    {0x50, "OnTrack DM"},
    {0x51, "OnTrack DM6 Aux1"},
    {0x52, "CP/M"},
    {0x53, "OnTrack DM6 Aux3"},
    {0x54, "OnTrack DM6 DDO"},
    {0x55, "EZ-Drive"},
    {0x56, "Golden Bow"},
    {0x57, "DrivePro"},
    {0x58, 0},
    {0x59, 0},
    {0x5A, 0},
    {0x5B, 0},
    {0x5C, "Priam EDisk"},
    {0x5D, 0},
    {0x5E, 0},
    {0x5F, 0},
    {0x60, 0},
    {0x61, "SpeedStor"},
    {0x62, 0},
    {0x63, "GNU Hurd/System V"},
    {0x64, "Novell Netware 286"},
    {0x65, "Novell Netware 386"},
    {0x66, "Novell Netware SMS"},
    {0x67, "Novell"},
    {0x68, "Novell"},
    {0x69, "Novell Netware 5+"},
    {0x6A, 0},
    {0x6B, 0},
    {0x6C, 0},
    {0x6D, 0},
    {0x6E, 0},
    {0x6F, 0},
    {0x70, "DiskSecure MultiBoot"},
    {0x71, 0},
    {0x72, "V7/x86"},
    {0x73, 0},
    {0x74, 0},
    {0x75, "PX/IX"},
    {0x76, 0},
    {0x77, "M2FS/M2CS"},
    {0x77, "VNDI"},
    {0x78, "XOSL FS"},
    {0x79, 0},
    {0x7A, 0},
    {0x7B, 0},
    {0x7C, 0},
    {0x7D, 0},
    {0x7E, 0},
    {0x7F, 0},
    {0x80, "Minix <1.4a"},
    {0x81, "Minix>1.4b/Old Linux"},
    {0x82, "Linux swap/Solaris"},
    {0x83, "Linux"},
    {0x84, "OS/2 hidden C:"},
    {0x85, "Linux extended"},
    {0x86, "FAT16 volume set"},
    {0x87, "NTFS volume set"},
    {0x88, "Linux plaintext"},
    {0x89, 0},
    {0x8A, 0},
    {0x8B, 0},
    {0x8C, 0},
    {0x8D, "FreeDOS hid'n FAT12"},
    {0x8E, "Linux LVM"},
    {0x8F, 0},
    {0x90, "FreeDOS hid'n FAT16"},
    {0x91, "FreeDOS hid'n ext'd"},
    {0x92, "FreeDOS hid'n FAT16"},
    {0x93, "Amoeba"},
    {0x94, "Amoeba BBT"},
    {0x95, "MIT EXOPC"},
    {0x96, 0},
    {0x97, "FreeDOS hid'n FAT32"},
    {0x98, "FreeDOS hid FAT32LBA"},
    {0x99, 0},
    {0x9A, "FreeDOS hid FAT16LBA"},
    {0x9B, "FreeDOS hid ext'dLBA"},
    {0x9C, 0},
    {0x9D, 0},
    {0x9E, 0},
    {0x9F, "BSD/OS"},
    {0xA0, "Thinkpad hibernate"},
    {0xA1, 0},
    {0xA2, 0},
    {0xA3, 0},
    {0xA4, 0},
    {0xA5, "NetBSD/FreeBSD"},
    {0xA6, "OpenBSD"},
    {0xA7, "MeXTStep"},
    {0xA8, "Darwin UFS"},
    {0xA9, "NetBSD"},
    {0xAA, 0},
    {0xAB, "Darwin boot"},
    {0xAC, 0},
    {0xAD, 0},
    {0xAE, "ShagOS"},
    {0xAF, "ShagOS swap"},
    {0xB0, 0},
    {0xB1, 0},
    {0xB2, 0},
    {0xB3, 0},
    {0xB4, 0},
    {0xB5, 0},
    {0xB6, 0},
    {0xB7, "BSDI BSD/386"},
    {0xB8, "BSDI BSD swap"},
    {0xB9, 0},
    {0xBA, 0},
    {0xBB, "BootWiz hid'n"},
    {0xBC, 0},
    {0xBD, 0},
    {0xBE, "Solaris 8 boot"},
    {0xBF, "Solaris x86"},
    {0xC0, "CTOS/DRDOS"},
    {0xC1, "DRDOS sec'd FAT12"},
    {0xC2, "Hidden Linux"},
    {0xC3, "Hid'n Linux swap"},
    {0xC4, "DRDOS sec FAT16 <32M"},
    {0xC5, "DRDOS sec'd ext'd"},
    {0xC6, "DRDOS sec FAT16 >32M"},
    {0xC7, "Syrinx"},
    {0xC8, 0},
    {0xC9, 0},
    {0xCA, 0},
    {0xCB, "DRDOS sec'd FAT32"},
    {0xCC, "DRDOS sec FAT32 LBA"},
    {0xCD, 0},
    {0xCE, "DRDOS sec FAT16 LBA"},
    {0xCF, "DRDOS sec ext'd LBA"},
    {0xD0, 0},
    {0xD1, 0},
    {0xD2, 0},
    {0xD3, 0},
    {0xD4, 0},
    {0xD5, 0},
    {0xD6, 0},
    {0xD7, 0},
    {0xD8, "CP/M-86"},
    {0xD9, 0},
    {0xDA, "Non-FS data"},
    {0xDB, "CP/M / CTOS"},
    {0xDC, 0},
    {0xDD, 0},
    {0xDE, "Dell PowerEdge"},
    {0xDF, "BootIt"},
    {0xE0, 0},
    {0xE1, "DOS access"},
    {0xE2, 0},
    {0xE3, "DOS R/O"},
    {0xE4, "SpeedStor"},
    {0xE5, "Tandy MDOS"},
    {0xE6, 0},
    {0xE7, 0},
    {0xE8, "LUKS"},
    {0xE9, 0},
    {0xEA, 0},
    {0xEB, "BeOS BFS"},
    {0xEC, "SkyOS SkyFS"},
    {0xED, 0},
    {0xEE, "EFI GPT"},
    {0xEF, "EFI"},
    {0xF0, "Linux/PA-RISC boot"},
    {0xF1, "SpeedStor"},
    {0xF2, "DOS 3.3+ secondary"},
    {0xF3, 0},
    {0xF4, "SpeedStor"},
    {0xF5, 0},
    {0xF6, "SpeedStor"},
    {0xF7, 0},
    {0xF8, 0},
    {0xF9, "pCache"},
    {0xFA, "Bochs"},
    {0xFB, "VMware FS"},
    {0xFC, "VMware swap"},
    {0xFD, "Linux raid auto"},
    {0xFE, "LANstep"},
    {0xFF, "Xenix BBT"}
};
