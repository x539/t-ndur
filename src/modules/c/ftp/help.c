/**
 * Copyright (c) 2009 The tyndur Project. All rights reserved.
 *
 * This code is derived from software contributed to the tyndur Project
 * by Paul Lange.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *     This product includes software developed by the tyndur Project
 *     and its contributors.
 * 4. Neither the name of the tyndur Project nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

const char* help =
    "FTP-Client Befehlsliste:\n\n"
    "ascii   - ascii Mode einschalten\n"
    "binary  - binary Mode einschalten\n"
    "bye     - schließt die Verbindung zum Server und beendet den FTP-Client\n"
    "cd      - [Pfad] wechseln des Verzeichnisses auf dem Server\n"
    "cdup    - wechselt zum Stammverzeichniss auf dem Server\n"
    "clear   - löscht den Inhalt des Terminals\n"
    "close   - schließt die Verbindung zum Server\n"
    "get     - [Pfad+Dateiname] holt eine Datei vom Server\n"
    "help    - zeigt diese Hilfe an\n"
    "mkdir   - [Pfad+Ordnername] erstellt einen Ordner auf dem Server\n"
    "open    - [ftp.name.net] öffnet eine Verbindung zu einem Server\n"
    "put     - [Pfad+Dateiname] speichert eine Datei auf dem Server\n"
    "pwd     - zeigt das akutell geoeffnete Verzeichniss auf dem Server an\n"
    "rm      - [Pfad+Dateiname] löscht die Datei auf dem Server\n"
    "rmdir   - [Pfad+Verzeichnisname] löscht ein Verzeichnis auf dem Server\n"
    "system  - zeigt den Namen des Betriebssystems vom Server an\n"
    "user    - einloggen mit Benutzername und Passwort\n";
