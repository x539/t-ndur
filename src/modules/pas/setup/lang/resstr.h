/*
 * Copyright (c) 2009 The tyndur Project. All rights reserved.
 *
 * This code is derived from software contributed to the tyndur Project
 * by Kevin Wolf.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RESSTR_H
#define RESSTR_H

extern void* RESSTR_P$SETUP_RSTITLE;

extern void* RESSTR_MENU_RSCHOOSEMODULE;
extern void* RESSTR_MENU_RSMODKEYBOARD;
extern void* RESSTR_MENU_RSMODLPT;
extern void* RESSTR_MENU_RSMODNETWORK;
extern void* RESSTR_MENU_RSQUIT;

extern void* RESSTR_SETUP_KEYBOARD_RSTITLE;
extern void* RESSTR_SETUP_KEYBOARD_RSGERMAN;
extern void* RESSTR_SETUP_KEYBOARD_RSSWISSGERMAN;
extern void* RESSTR_SETUP_KEYBOARD_RSUSAMERICAN;
extern void* RESSTR_SETUP_KEYBOARD_RSBACK;
extern void* RESSTR_SETUP_KEYBOARD_RSCHOOSELAYOUT;

extern void* RESSTR_SETUP_LPT_RSTITLE;
extern void* RESSTR_SETUP_LPT_RSANYKEY;
extern void* RESSTR_SETUP_LPT_RSCHOOSE;
extern void* RESSTR_SETUP_LPT_RSSTABLE;
extern void* RESSTR_SETUP_LPT_RSCURRENT;
extern void* RESSTR_SETUP_LPT_RSBACK;

extern void* RESSTR_SETUP_NETWORK_RSTITLE;
extern void* RESSTR_SETUP_NETWORK_RSCONFIGURENETWORK;
extern void* RESSTR_SETUP_NETWORK_RSDRIVER;
extern void* RESSTR_SETUP_NETWORK_RSDHCP;
extern void* RESSTR_SETUP_NETWORK_RSSTATIC;
extern void* RESSTR_SETUP_NETWORK_RSIPADDRESS;
extern void* RESSTR_SETUP_NETWORK_RSGATEWAY;
extern void* RESSTR_SETUP_NETWORK_RSOK;
extern void* RESSTR_SETUP_NETWORK_RSCANCEL;

#endif
