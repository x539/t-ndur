type 
    size_t = dword;
    paddr_t = Pointer;
    vaddr_t = Pointer;

    pid_t = dword;
    uid_t = dword;

    timestamp_t = qword;
