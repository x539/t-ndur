#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{
    cout << "Starte C++ Tests" << endl;

    unsigned int u_int = 42424242;
    cout << " Unsigned integer:" << u_int << endl;
    signed int s_int = -42424242;
    cout << " Signed integer:" << s_int << endl;
    
    return 0;
}

